
#gem test-unit for testing
require 'test/unit'


# function to find three elements in an array whose sum is equal to a given number.
# Input parameters 1. array : list of elements
# 	               2. sum(int) : number to which sum of triplet should be matched4
# Output triplet i.e three numbers whose sum is equal to given sum

# Example Input: array = [25,10,7,5,2,4,8] and sum = 39
#         Output: 4 10 25

def triplet_equals_sum(array,sum)
	#if length of array is less than 3
	if array.empty? or array.length < 3
		return "Provide array with atleast 3 values"
	#if length of array is greater than 3
	else
		#sort given array
		array.sort!
		
		#initialize triplet as empty array so as to check in case of break statement
		triplet = []

		#index of first element
		start_index = 0

		#iterate over all the elemets of array fixinf first element
		while start_index < array.length - 2
			first_no = array[start_index]
			second_no = array[start_index + 1]
			third_no = array[start_index + 2]
			# triplet equals to sum found then break
			if first_no + second_no + third_no == sum
				triplet = first_no, second_no, third_no
				break
			# triplet not found
			else
				next_index = start_index + 1

				#find triplet keeping current element as second element of triplet
				while next_index < array.length-1
					second_no = array[next_index]
					third_no = array[next_index + 1]
					# triplet equals to sum found then break
					if first_no + second_no + third_no == sum
						triplet = first_no, second_no, third_no
						break
					end
					# increement next_index so as to move on to next element
					next_index+=1
				end
			end
			# if triplet is not empty
			if !triplet.empty?
				break
			end

			# increement start_index so as to move on to next element
			start_index+=1
		end
		# if triplet is not empty
		if !triplet.empty?
			return triplet
		# if no triplet found
		else
			return "No triplet found"
		end
	end
end

# Unit Testing
class TestTriplet < Test::Unit::TestCase
	def test_triplet_sum
		assert_equal triplet_equals_sum([2,5,3,6,7,1],12), [1,5,6]
		assert_equal triplet_equals_sum([25,10,5,4,9,6],39), [4,10,25]
		assert_equal triplet_equals_sum([],5), "Provide array with atleast 3 values"
		assert_equal triplet_equals_sum([-7,-4,2,-1,8],-6),[-7,-1,2]
		assert_equal triplet_equals_sum([2],7), "Provide array with atleast 3 values"
		assert_equal triplet_equals_sum([25,10,5,4,9,6],12), "No triplet found"
	end
end

