# libraries required
include Math
require 'json'
require "test/unit"

# function to calculate distance between source and destination on the basis of
# GPS coordinates
# Input parameters: source_address : array of coordinates of source location
# 				  destination_address : array of coordinates of destination location

# Output: distance betwween source and destination in kms
# Example Input: ([26.772340,68.12345],[34.75648,78.8694567])
#         Output: 1355.753073744086

def distance_calc(source_address,destination_address)

	#radius of earth
	earth_radius = 6371

	#conversion of source address coordinates to radian
	source_latitude = source_address[0].to_f * PI / 180 
	source_longitude = source_address[1].to_f * PI / 180 

	#conversion of destination address coordinates to radian
	destination_latitude = destination_address[0].to_f * PI / 180 
	destination_longitude = destination_address[1].to_f * PI / 180 

	#absolute difference of source and destination longitude
	longitude_diff = (destination_longitude - source_longitude).abs

	#expression for calculating distance
	sigma = acos(sin(source_latitude) * sin(destination_latitude) +
	                  cos(source_latitude) * cos(destination_latitude) *
	                  cos(longitude_diff))

	# final distance
	distance = earth_radius * sigma

	return distance
end

# function to check whether distance between office and friends address is less than
# 100 kms or not.
# Input prameters: office_address : coordinates of office address
#                friends_address: coordinates of friends address 
# Output : boolean : true if distance is less than or equal to 100 kms
#                    false if distance is greater than 100 kms

# Example Input : ([14.8411148, 73.3472276],[14.86234,73.367812])
#         Output : true

def is_within_100_kms(office_address,friends_address)
	# calculate distance between office addres and friends address
	distance = distance_calc(office_address,friends_address)

	# checks value of distance is less than or greater than 100
	if distance <= 100
	    return true
	else
		return false
	end
end

# function to create list of friends whose distance is less than 100 km to invite them
# Input parameters office_address : coordinates of office address
#                  friends_detail : sorted list of friends_records having distance less than 100

# Example Input : office_address : [12.8411148, 77.3472276]
# 				  friends_detail : [{"name"=>"P", "id"=>"104", "address"=>{"latitude"=>"16.71338487", "longitude"=>"74.263748"}}
# 								{"name"=>"Q", "id"=>"102", "address"=>{"latitude"=>"23.12345", "longitude"=>"79.102490"}}
# 								{"name"=>"R", "id"=>"108", "address"=>{"latitude"=>"12.465769", "longitude"=>"77.676354"}}
# 								{"name"=>"S", "id"=>"106", "address"=>{"latitude"=>"14.971384", "longitude"=>"77.4658950"}}]
#          Output : {"name"=>"R", "id"=>"108", "address"=>{"latitude"=>"12.465769", "longitude"=>"77.676354"}}

def friend_invite_list(office_address,friends_detail)
	invite_list=[]
	# iterate through each friend
	friends_detail.each do |friend|

		# friends address in form of latitude and longitude
		friends_address = [friend["address"]["latitude"].to_f,friend["address"]["longitude"].to_f]

		#append friend record into list if distance is less than or equal to 100
		if is_within_100_kms(office_address,friends_address)
			invite_list << friend
		end
	end
	# sort selected friend list
	sorted_friend_list = invite_list.sort_by { |friend| friend["id"] }

	# return sorted friend list
	return sorted_friend_list
end

def show_selected_friends(office_address,friends_hash)
	# friends list to invite
	invite_list = friend_invite_list(office_address,friends_hash)

	if invite_list.empty? or invite_list.nil?
		return "No friend within 100 km range"
	else
		selected_list = []
		invite_list.each do |friend|
			selected_list<<friend["id"] + "  " + friend["name"]
		end
		return selected_list
	end
end

# # main block

# puts "Enter full path of required file"
# friends_file_path = gets.chomp() #file name

# # open input file
# friends_file = File.read(friends_file_path)

# # parse JSON file to convert it into hash
# friends_hash = JSON.parse(friends_file)

# # office address
# puts "Enter office address coordinates seperated by space"
# office_address = gets.chomp.split()

# puts show_selected_friends(office_address,friends_hash)


# Testing
class FriendsInvitationTesting < Test::Unit::TestCase
	# to test distance calculation function
	def test_distance_calc
		assert_equal distance_calc([26.772340,68.12345],[34.75648,78.8694567]),1355.753073744086
		assert_equal distance_calc([12.123,75.26849],[12.345678,75.345678]), 26.14285211559117
	end

	# to test is_within_100_kms function
	def test_is_within_100_kms
		assert_equal is_within_100_kms([12.123,75.26849],[12.345678,75.345678]),true
		assert_equal is_within_100_kms([26.772340,68.12345],[34.75648,78.8694567]),false
	end

	# to test friend_invite_list function
	def test_friend_invite_list
		assert_equal friend_invite_list([12.8411148, 77.3472276],
		[{"name"=>"P", "id"=>"104", "address"=>{"latitude"=>"16.71338487", "longitude"=>"74.263748"}},
	    {"name"=>"Q", "id"=>"102", "address"=>{"latitude"=>"23.12345", "longitude"=>"79.102490"}}]),
	    []
	    assert_equal friend_invite_list([12.8411148, 77.3472276],
	    [{"name"=>"R", "id"=>"108", "address"=>{"latitude"=>"12.465769", "longitude"=>"77.676354"}},
 		{"name"=>"S", "id"=>"106", "address"=>{"latitude"=>"14.971384", "longitude"=>"77.4658950"}}]),
 		[{"name"=>"R", "id"=>"108", "address"=>{"latitude"=>"12.465769", "longitude"=>"77.676354"}}]
	end

	# to test show show_selected_friends
	def test_show_selected_friends
		assert_equal show_selected_friends([12.8411148, 77.3472276],
		[{"name"=>"P", "id"=>"104", "address"=>{"latitude"=>"16.71338487", "longitude"=>"74.263748"}},
	    {"name"=>"Q", "id"=>"102", "address"=>{"latitude"=>"23.12345", "longitude"=>"79.102490"}}]),
	    "No friend within 100 km range"

	    assert_equal show_selected_friends([12.8411148, 77.3472276],
	    [{"name"=>"R", "id"=>"108", "address"=>{"latitude"=>"12.465769", "longitude"=>"77.676354"}},
 		{"name"=>"S", "id"=>"106", "address"=>{"latitude"=>"14.971384", "longitude"=>"77.4658950"}}]),
 		["108  R"]
	end

end
